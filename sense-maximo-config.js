module.exports = function(RED) {
    function RemoteServerNode(n) {
        RED.nodes.createNode(this,n);
        this.host = (n.host.substr(-1) == "/"? n.host : n.host + "/") + "maximo/rest/os/";
        this.apiKey = n.apiKey;
    }
    RED.nodes.registerType("maximo-config",RemoteServerNode);
}