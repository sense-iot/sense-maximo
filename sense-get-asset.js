module.exports = function(RED) 
{
    function CheckVal(config) 
    {
        RED.nodes.createNode(this, config);
        var node = this;
        var XMLHttpRequest = require("xhr2").XMLHttpRequest;
        var parseString = require('xml2js').parseString;

        this.maximo = RED.nodes.getNode(config.maximo);

        node.on('input', function(msg) 
        {
            let Http = new XMLHttpRequest();
            let url= this.maximo.host + config.objectStructureType + "?ASSETNUM=" + msg.payload;

            Http.open("GET", url);
            Http.setRequestHeader("maxauth", this.maximo.apiKey);
            Http.send();
            
            Http.onreadystatechange = (e) => 
            {
                if(e.target.readyState == 4)
                {
                    if(e.target.status == 200)
                    {
                        
                        var xml = Http.responseText;
                        parseString(xml, function (err, result) 
                        {
                            let assetDetail = result.QuerySNDASSET2Response[config.objectStructureType + "Set"][0].ASSET[0];

                            msg.payload = {};

                            let unArray = function(toDo, isArray)
                            {
                                let result = isArray? [] : {};
                                Object.entries(toDo).forEach(entry => 
                                {
                                    if(entry[0] != "$")
                                    {
                                        if(entry[1].length == 1)
                                            result[entry[0]] = isNaN(entry[1][0])? entry[1][0]: Number(entry[1][0]);
                                        else
                                        {
                                            result[entry[0]] = unArray(entry[1], true);
                                        }
                                    }
                                });

                                return result;
                            }

                            msg.payload = unArray(assetDetail, false);

                            node.send(msg);
                        });
                    }
                    else
                    {
                        node.error("Received " + e.target.status + " response: " + e.target.responseText, e);
                        console.error("Received " + e.target.status + " response: " + e.target.responseText, e);
                    }
                }
            }
        });
    }

    RED.nodes.registerType("get-asset", CheckVal);
}